<?php

use Illuminate\Support\Facades\Route;

use Auth\Infrastructure\Controllers\AuthController;
use Auth\Infrastructure\Controllers\UserController;

Route::prefix('')->group(function () {
    Route::post('login', [AuthController::class, 'login'])->name('auth.login');
});

Route::prefix('users')->group(function () {
    Route::get('get-by-bearer-token', [UserController::class, 'getByBearerToken'])
        ->name('users.getByBearerToken');
});
