<?php

namespace Auth\Test\Application\Services;

use Auth\Application\Interfaces\Services\AuthManagerServiceInterface;
use Auth\Application\Interfaces\Services\UserServiceInterface;
use Auth\Application\Mocks\Services\AuthManagerServiceMock;
use Auth\Infrastructure\Interfaces\Repositories\System\UserRepositoryInterface;
use Auth\Infrastructure\Mocks\Repositories\UserRepositoryMock;
use Auth\Test\Base;
use Illuminate\Support\Facades\App;

class UserServiceTest extends Base
{
    protected function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @test
     */
    public function isGetByTokenWorking()
    {
        $this->instance(
            AuthManagerServiceInterface::class,
            (App::make(AuthManagerServiceMock::class))->generateDecodeTokenWorking()
        );

        $this->instance(
            UserRepositoryInterface::class,
            (App::make(UserRepositoryMock::class))->generateFindByEmailWorking()
        );

        $user = (App::make(UserServiceInterface::class))
            ->getByToken('token');

        $this->assertNotNull($user);
    }

    /**
     * @test
     */
    public function isGetByTokenFailing()
    {
        $this->instance(
            AuthManagerServiceInterface::class,
            (App::make(AuthManagerServiceMock::class))->generateDecodeTokenWorking()
        );

        $this->instance(
            UserRepositoryInterface::class,
            (App::make(UserRepositoryMock::class))->generateFindByEmailFailing()
        );

        try {
            (App::make(UserServiceInterface::class))
                ->getByToken('token');
        } catch (\Exception $exception) {
            $this->assertTrue($exception->getCode() === 404);
        }
    }
}
