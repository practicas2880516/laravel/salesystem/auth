<?php

namespace Auth\Test\Application\Services;

use Auth\Application\Interfaces\Services\AuthManagerServiceInterface;
use Auth\Domain\Dto\Auth\LoginDto;
use Auth\Test\Base;
use Illuminate\Support\Facades\App;

class AuthManagerServiceTest extends Base
{
    /**
     * @test
     */
    public function isLoginWorking()
    {
        $dto = new LoginDto();
        $dto->email = 'jhanvega01@outlook.com';
        $dto->password = 'password';

        (App::make(AuthManagerServiceInterface::class))
            ->login($dto);

        $this->assertNotNull(auth()->user());
    }

    /**
     * @test
     */
    public function isGenerateTokenWorking()
    {
        $dto = new LoginDto();
        $dto->email = 'jhanvega01@outlook.com';
        $dto->password = 'password';

        $token = (App::make(AuthManagerServiceInterface::class))
            ->login($dto)
            ->generateToken();

        $this->assertNotNull($token);
    }

    /**
     * @test
     */
    public function isDecodeTokenWorking()
    {
        $this->setNewUser();

        $obj = (App::make(AuthManagerServiceInterface::class))
            ->decodeToken($this->generateToken());

        $this->assertNotNull($obj);
    }
}
