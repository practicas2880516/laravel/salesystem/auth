<?php

namespace Auth\Test;

use App\Models\User;
use Firebase\JWT\JWT;
use Tests\TestCase;

class Base extends TestCase
{
    /**
     * @var User
     */
    protected User $user;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @return void
     */
    protected function setNewUser():void
    {
        $this->user = new User();
        $this->user->id = 1;
    }

    protected function generateToken()
    {
        return JWT::encode($this->user->toArray(), 'Test', 'HS256');
    }
}
