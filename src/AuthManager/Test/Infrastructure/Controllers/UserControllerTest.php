<?php

namespace Auth\Test\Infrastructure\Controllers;

use Auth\Application\Interfaces\Services\UserServiceInterface;
use Auth\Application\Mocks\Services\UserServiceMock;
use Auth\Test\Base;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;

class UserControllerTest extends Base
{
    protected function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @test
     */
    public function isGetByBearerTokenWorking()
    {
        $this->instance(
            UserServiceInterface::class,
            (App::make(UserServiceMock::class))->generateGetByTokenWorking()
        );

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . Str::random(30)
        ])->get(route('users.getByBearerToken'));

        $response->assertStatus(200);
        $response->assertJsonStructure(['success', 'code', 'message', 'user']);
    }
}
