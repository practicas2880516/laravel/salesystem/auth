<?php

namespace Auth\Test\Infrastructure\Controllers;

use Auth\Application\Interfaces\Services\AuthManagerServiceInterface;
use Auth\Application\Mocks\Services\AuthManagerServiceMock;
use Auth\Test\Base;
use Illuminate\Support\Facades\App;

class AuthControllerTest extends Base
{
    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @test
     */
    public function isLoginWorking()
    {
        $this->instance(
            AuthManagerServiceInterface::class,
            (App::make(AuthManagerServiceMock::class))->generateLoginWorking()
        );

        $response = $this->post(route('auth.login'), [
            'email' => 'admin@admin',
            'password' => 'password'
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure(['success', 'code', 'message', 'token']);
    }

}
