<?php

namespace Auth\Domain\Exceptions;

class UserNotFoundException extends \Exception
{
    /**
     * @var int
     */
    protected $code = 404;

    /**
     * @var string
     */
    protected $message = 'User not found';
}
