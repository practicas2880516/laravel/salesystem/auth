<?php

namespace Auth\Infrastructure\Mocks\Repositories;

use Auth\Infrastructure\Interfaces\Repositories\System\UserRepositoryInterface;

class UserRepositoryMock
{
    public function generateFindByEmailWorking()
    {
        $mock = \Mockery::mock(UserRepositoryInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('findByEmail')
            ->once()
            ->andReturnUsing(function () {
                $obj = new \stdClass();
                $obj->id = 1;
                $obj->name = 'Admin';
                $obj->email = 'admin@admin';
                return $obj;
            });

        return $mock;
    }

    public function generateFindByEmailFailing()
    {
        $mock = \Mockery::mock(UserRepositoryInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('findByEmail')
            ->once()
            ->andReturnNull();

        return $mock;
    }
}
