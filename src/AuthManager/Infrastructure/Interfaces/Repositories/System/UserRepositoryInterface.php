<?php

namespace Auth\Infrastructure\Interfaces\Repositories\System;

use Auth\Infrastructure\Interfaces\Repositories\BaseRepositoryInterface;

interface UserRepositoryInterface extends BaseRepositoryInterface
{
    /**
     * @param string $email
     * @return object|null
     */
    public function findByEmail(string $email):object|null;
}
