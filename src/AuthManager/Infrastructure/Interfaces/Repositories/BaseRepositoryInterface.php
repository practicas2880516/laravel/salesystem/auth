<?php

namespace Auth\Infrastructure\Interfaces\Repositories;

interface BaseRepositoryInterface
{
    /**
     * @return string
     */
    public function getTableName():string;

    /**
     * @return string
     */
    public function getDatabaseConnection():string;
}
