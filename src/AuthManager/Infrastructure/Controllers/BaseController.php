<?php

namespace Auth\Infrastructure\Controllers;

use App\Http\Controllers\Controller;

class BaseController extends Controller
{
    public function executeWithJsonSuccessResponse($callback)
    {
        try{
            $response = $callback();
            $response = array_merge([
                'success'=> true,
                'code'=> 200,
                'message'=> ''
            ], $response);
        }catch (\Exception $exception){
            report($exception);
            $code = (int) $exception->getCode() < 100 || $exception->getCode() > 599 ? 500 : $exception->getCode();
            $response = response()->json([
                'success'=> false,
                'code'=> $exception->getCode(),
                'message'=> $exception->getMessage()
            ], $code);
        }
        return $response;
    }
}
