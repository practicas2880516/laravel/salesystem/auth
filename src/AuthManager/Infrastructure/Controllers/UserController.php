<?php

namespace Auth\Infrastructure\Controllers;

use Auth\Application\Interfaces\Services\UserServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class UserController extends BaseController
{
    /**
     * @var UserServiceInterface
     */
    private UserServiceInterface $userService;

    /**
     * @param UserServiceInterface $userService
     */
    public function __construct(
        UserServiceInterface $userService
    )
    {
        $this->userService = $userService;
    }

    public function getByBearerToken(Request $request)
    {
        return $this->executeWithJsonSuccessResponse(function () use ($request) {

            $user = $this->userService
                ->getByToken($request->bearerToken());

            return [
                'message' => 'User found',
                'user' => $user
            ];
        });
    }
}
