<?php

namespace Auth\Infrastructure\Controllers;

use Auth\Application\Interfaces\Services\AuthManagerServiceInterface;
use Auth\Application\Mappers\Auth\LoginDtoMapper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class AuthController extends BaseController
{
    private AuthManagerServiceInterface $authManagerService;

    public function __construct(
        AuthManagerServiceInterface $authManagerService
    )
    {
        $this->authManagerService = $authManagerService;
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required', 'string']
        ]);

        return $this->executeWithJsonSuccessResponse(function () use ($request) {

            $dto = (App::make(LoginDtoMapper::class))
                ->createFromRequest($request);

            $token = $this->authManagerService
                ->login($dto)
                ->generateToken();

            return [
                'message' => 'Login successful',
                'token' => $token
            ];
        });
    }
}
