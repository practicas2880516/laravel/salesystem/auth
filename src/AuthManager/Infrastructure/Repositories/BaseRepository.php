<?php

namespace Auth\Infrastructure\Repositories;

use Auth\Infrastructure\Interfaces\Repositories\BaseRepositoryInterface;

class BaseRepository implements BaseRepositoryInterface
{
    protected ?object $user = null;

    /**
     * @var string
     */
    protected string $tableName;

    /**
     * @var string
     */
    protected string $databaseConnection;

    /**
     * @return string
     */
    public function getTableName():string
    {
        return $this->tableName;
    }

    /**
     * @return string
     */
    public function getDatabaseConnection():string
    {
        return $this->databaseConnection;
    }
}
