<?php

namespace Auth\Infrastructure\Repositories\System;

use Auth\Infrastructure\Interfaces\Repositories\System\UserRepositoryInterface;
use Auth\Infrastructure\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    /**
     * @var string
     */
    protected string $tableName = 'auth.users';

    /**
     * @var string
     */
    protected string $databaseConnection = 'pgsql';

    /**
     * @param string $email
     * @return object|null
     */
    public function findByEmail(string $email):object|null
    {
        return DB::connection($this->databaseConnection)
            ->table($this->tableName)
            ->select([
                'id AS id',
                'name AS name',
                'email AS email',
            ])
            ->where('email', '=', $email)
            ->first();
    }

}
