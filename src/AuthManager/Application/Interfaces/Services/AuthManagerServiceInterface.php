<?php

namespace Auth\Application\Interfaces\Services;

use Auth\Domain\Dto\Auth\LoginDto;

interface AuthManagerServiceInterface
{
    /**
     * @param LoginDto $dto
     * @return self
     */
    public function login(LoginDto $dto):self;

    /**
     * @return string
     */
    public function generateToken():string;

    /**
     * @param string $token
     * @return object
     */
    public function decodeToken(string $token):object;
}
