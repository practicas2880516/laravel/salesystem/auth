<?php

namespace Auth\Application\Interfaces\Services;

interface UserServiceInterface
{
    /**
     * @param string $token
     * @return object
     */
    public function getByToken(string $token):object;
}
