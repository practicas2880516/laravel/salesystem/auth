<?php

namespace Auth\Application\Services;

use Auth\Application\Interfaces\Services\AuthManagerServiceInterface;
use Auth\Domain\Dto\Auth\LoginDto;
use Auth\Domain\Exceptions\UserNotFoundException;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;

class AuthManagerService implements AuthManagerServiceInterface
{
    /**
     * @var string
     */
    private string $JWTKey;

    private array $payload = [];

    public function __construct()
    {
        $this->JWTKey = env('JWT_SECRET');
    }

    /**
     * @param LoginDto $dto
     * @return $this
     */
    public function login(LoginDto $dto): self
    {
        auth()->attempt($dto->getAttributes());

        throw_if(is_null(auth()->user()), new UserNotFoundException());

        $this->payload = auth()->user()->getJWTClaims();
        return $this;
    }

    /**
     * @param array $claims
     * @return string
     */
    public function generateToken():string
    {
        return JWT::encode($this->payload, $this->JWTKey, 'HS256');
    }

    /**
     * @param string $token
     * @return object
     */
    public function decodeToken(string $token):object
    {
        return JWT::decode($token, new Key($this->JWTKey, 'HS256'));
    }
}
