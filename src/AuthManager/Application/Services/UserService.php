<?php

namespace Auth\Application\Services;

use Auth\Application\Interfaces\Services\AuthManagerServiceInterface;
use Auth\Application\Interfaces\Services\UserServiceInterface;
use Auth\Domain\Exceptions\UserNotFoundException;
use Auth\Infrastructure\Interfaces\Repositories\System\UserRepositoryInterface;

class UserService implements UserServiceInterface
{
    /**
     * @var AuthManagerServiceInterface
     */
    private AuthManagerServiceInterface $authManagerService;

    /**
     * @var UserRepositoryInterface
     */
    private UserRepositoryInterface $userRepo;

    /**
     * @param AuthManagerServiceInterface $authManagerService
     * @param UserRepositoryInterface $userRepo
     */
    public function __construct(
        AuthManagerServiceInterface $authManagerService,
        UserRepositoryInterface $userRepo
    )
    {
        $this->authManagerService = $authManagerService;
        $this->userRepo = $userRepo;
    }

    /**
     * @param string $token
     * @return object
     * @throws \Throwable
     */
    public function getByToken(string $token): object
    {
        $userToken = $this->authManagerService
            ->decodeToken($token);

        $user = $this->userRepo
            ->findByEmail($userToken->email);

        throw_if(is_null($user), new UserNotFoundException());

        return $user;
    }

}
