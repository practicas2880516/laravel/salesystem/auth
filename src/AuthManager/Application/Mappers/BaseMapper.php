<?php

namespace Auth\Application\Mappers;

use Auth\Domain\Dto\BaseDto;

abstract class BaseMapper
{
    /**
     * @var BaseDto
     */
    protected BaseDto $dto;

    /**
     * @return BaseDto
     */
    abstract protected function getNewDto():BaseDto;
}
