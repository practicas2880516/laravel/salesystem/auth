<?php

namespace Auth\Application\Mappers\Auth;

use Auth\Application\Mappers\BaseMapper;
use Auth\Domain\Dto\Auth\LoginDto;
use Auth\Domain\Dto\BaseDto;
use Illuminate\Http\Request;

class LoginDtoMapper extends BaseMapper
{
    protected function getNewDto(): LoginDto
    {
        return new LoginDto();
    }

    /**
     * @param Request $request
     * @return LoginDto
     */
    public function createFromRequest(Request $request):LoginDto
    {
        $dto = $this->getNewDto();
        $dto->email = $request->get('email');
        $dto->password = $request->get('password');
        return $dto;
    }
}
