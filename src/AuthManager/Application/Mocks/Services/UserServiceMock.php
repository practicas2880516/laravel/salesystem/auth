<?php

namespace Auth\Application\Mocks\Services;

use Auth\Application\Interfaces\Services\UserServiceInterface;

class UserServiceMock
{
    public function generateGetByTokenWorking()
    {
        $mock = \Mockery::mock(UserServiceInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('getByToken')
            ->once()
            ->andReturnUsing(function () {
                $obj = new \stdClass();
                $obj->id = 1;
                $obj->name = 'Admin';
                $obj->email = 'admin@admin';
                return $obj;
            });

        return $mock;
    }
}
