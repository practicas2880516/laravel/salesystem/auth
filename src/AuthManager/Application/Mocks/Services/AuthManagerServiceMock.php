<?php

namespace Auth\Application\Mocks\Services;

use Auth\Application\Interfaces\Services\AuthManagerServiceInterface;
use Illuminate\Support\Str;

class AuthManagerServiceMock
{
    public function generateLoginWorking()
    {
        $mock = \Mockery::mock(AuthManagerServiceInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('login')
            ->once()
            ->andReturnSelf();

        $mock->shouldReceive('generateToken')
            ->once()
            ->andReturn(Str::random(30));

        return $mock;
    }

    public function generateDecodeTokenWorking()
    {
        $mock = \Mockery::mock(AuthManagerServiceInterface::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('decodeToken')
            ->once()
            ->andReturnUsing(function () {
                $obj = new \stdClass();
                $obj->id = 1;
                $obj->email = 'admin@admin';
                return $obj;
            });

        return $mock;
    }
}
