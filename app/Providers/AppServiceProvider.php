<?php

namespace App\Providers;

use Auth\Application\Interfaces\Services\AuthManagerServiceInterface;
use Auth\Application\Interfaces\Services\UserServiceInterface;
use Auth\Application\Services\AuthManagerService;
use Auth\Application\Services\UserService;
use Auth\Infrastructure\Interfaces\Repositories\System\UserRepositoryInterface;
use Auth\Infrastructure\Repositories\System\UserRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {

        // Services
        $this->app->bind(AuthManagerServiceInterface::class, AuthManagerService::class);
        $this->app->bind(UserServiceInterface::class, UserService::class);

        // Repositories
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);

    }
}
